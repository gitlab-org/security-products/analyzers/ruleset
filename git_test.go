package ruleset

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/go-git/go-git/v5/plumbing"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCloneGit(t *testing.T) {
	var tests = []struct {
		name         string
		passthrough  *Passthrough
		authenticate bool
		wantErr      string
	}{
		{
			name: "Valid Unauthenticated URI with default ref",
			passthrough: &Passthrough{
				Value: "gitlab.com/gitlab-org/security-products/sast-rules",
				Ref:   "",
			},
		},
		{
			name: "Valid Unauthenticated URI with short branch ref",
			passthrough: &Passthrough{
				Value: "gitlab.com/gitlab-org/security-products/sast-rules",
				Ref:   "main",
			},
		},
		{
			name: "Valid Unauthenticated URI with unambiguous branch ref",
			passthrough: &Passthrough{
				Value: "gitlab.com/gitlab-org/security-products/sast-rules",
				Ref:   "refs/heads/main",
			},
		},
		{
			name: "Valid Unauthenticated URI with unambiguous tag ref",
			passthrough: &Passthrough{
				Value: "gitlab.com/gitlab-org/security-products/sast-rules",
				Ref:   "refs/tags/v1.3.6",
			},
		},
		{
			name: "Valid Unauthenticated URI with hash",
			passthrough: &Passthrough{
				Value: "gitlab.com/gitlab-org/security-products/sast-rules",
				Ref:   "086e7c579f2b141eb67005e62a6ed4b7c46be5ad",
			},
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			tmpdir, err := ioutil.TempDir("/tmp", "test")
			require.NoError(t, err)
			defer os.RemoveAll(tmpdir)

			tt.passthrough.Target = tmpdir

			gitSource, err := cloneGit(*tt.passthrough, Logger{})
			if err != nil {
				if !tt.authenticate {
					assert.Error(t, err)
					assert.Contains(t, err.Error(), "authentication required")
					return
				}

				if tt.wantErr == "" {
					t.Fatal(err)
				}
			}
			defer os.RemoveAll(gitSource)
		})
	}
}

func TestResolveRemoteReference(t *testing.T) {
	var tests = []struct {
		name       string
		url        string
		ref        string
		wantErr    bool
		wantBranch bool
		wantHead   bool
		wantHash   bool
		wantTag    bool
	}{
		{
			name:     "Valid Unauthenticated URI with default ref",
			url:      "https://gitlab.com/gitlab-org/security-products/sast-rules",
			ref:      "",
			wantHead: true,
		},
		{
			name:       "Valid Unauthenticated URI with short branch ref",
			url:        "https://gitlab.com/gitlab-org/security-products/sast-rules",
			ref:        "main",
			wantBranch: true,
		},
		{
			name:    "Valid Unauthenticated URI with short tag ref",
			url:     "https://gitlab.com/gitlab-org/security-products/sast-rules",
			ref:     "v1.3.6",
			wantTag: true,
		},
		{
			name:       "Valid Unauthenticated URI with unambiguous branch ref",
			url:        "https://gitlab.com/gitlab-org/security-products/sast-rules",
			ref:        "refs/heads/main",
			wantBranch: true,
		},
		{
			name:    "Valid Unauthenticated URI with unambiguous tag ref",
			url:     "https://gitlab.com/gitlab-org/security-products/sast-rules",
			ref:     "refs/tags/v1.3.6",
			wantTag: true,
		},
		{
			name: "Valid Unauthenticated URI with hash",

			url:      "https://gitlab.com/gitlab-org/security-products/sast-rules",
			ref:      "086e7c579f2b141eb67005e62a6ed4b7c46be5ad",
			wantHash: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			got, err := resolveRemoteReference(tt.url, tt.ref, "")

			assert.NoError(t, err)
			if tt.wantHead {
				assert.Equal(t, plumbing.HEAD, got)
			}
			assert.Equal(t, tt.wantBranch, got.IsBranch())
			if tt.wantHash {
				assert.Equal(t, tt.ref, got.String())
			}
			assert.Equal(t, tt.wantTag, got.IsTag())
		})
	}
}
