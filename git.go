package ruleset

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/storage/memory"
	"github.com/otiai10/copy"
)

const (
	// GitProtocol is currently limited to cloning over https only
	GitProtocol = "https://"
)

// cloneGit clones a remote git repository and returns the target path
func cloneGit(passthrough Passthrough, logger GenericLogger) (string, error) {
	// target directory of the git repo
	gitSource, err := ioutil.TempDir(os.TempDir(), "glsastruleset")
	if err != nil {
		return "", err
	}
	defer os.RemoveAll(gitSource)

	ref, err := resolveRemoteReference(passthrough.Value, passthrough.Ref, passthrough.Auth)
	if err != nil {
		return "", fmt.Errorf("cloneGit error: %w", err)
	}

	if passthrough.Ref == "" {
		err = handleGitBranch(passthrough, gitSource, logger)
	} else if ref.IsBranch() || ref.IsTag() {
		passthrough.Ref = ref.String()
		err = handleGitBranch(passthrough, gitSource, logger)
	} else {
		err = handleGitSha(passthrough, gitSource, logger)
	}
	if err != nil {
		return "", fmt.Errorf("cloneGit error: %w", err)
	}

	if passthrough.Subdir != "" {
		gitSource = filepath.Join(gitSource, cleanPath(passthrough.Subdir))
	}

	logger.Debugf("Copy %s to %s", gitSource, passthrough.Target)
	// make sure that we do not copy over any information from the git history
	return passthrough.Target, copy.Copy(gitSource, passthrough.Target, copy.Options{
		Skip: func(_ os.FileInfo, src, _ string) (bool, error) {
			rel, err := filepath.Rel(gitSource, src)
			if err != nil {
				return false, err
			}
			return rel == ".git", nil
		},
	})
}

// resolveRemoteReference accepts a url, reference string, and auth string
// and attempts to resolve a remote git reference using `git ls-remote`.
// If reference string corresponds to the short name for a Reference,
// the unambiguous ReferenceName is returned.
func resolveRemoteReference(url string, refString string, auth string) (plumbing.ReferenceName, error) {
	if refString == "" {
		// No ref specified. Assume user is interested in the tip of the repo.
		return plumbing.HEAD, nil
	}

	if plumbing.IsHash(refString) || strings.HasPrefix(refString, "refs/") {
		// An unambiguous ref specified, no need for extra work.
		return plumbing.ReferenceName(refString), nil
	}

	// An ambiguous ref specified. We need to derive the associated _unambiguous_ ref.
	url = formatGitURI(url, auth)
	remote := git.NewRemote(memory.NewStorage(), &config.RemoteConfig{
		Name: "origin",
		URLs: []string{url},
	})

	refs, err := remote.List(&git.ListOptions{})
	if err != nil {
		return "", err
	}

	for _, ref := range refs {
		if ref.Name().Short() == refString {
			return ref.Name(), nil
		}
	}

	return "", fmt.Errorf("Could not resolve ref: %v", refString)
}

// avoid printing out URL and Auth since they could contain secrets
func handleGitSha(passthrough Passthrough, dir string, logger GenericLogger) error {
	logger.Debugf("Cloning and checking out %s to %s", passthrough.Ref, dir)

	url := formatGitURI(passthrough.Value, passthrough.Auth)

	g, err := git.PlainClone(dir, false, &git.CloneOptions{URL: url})
	if err != nil {
		return err
	}

	wtree, err := g.Worktree()
	if err != nil {
		return err
	}

	if err = wtree.Checkout(&git.CheckoutOptions{
		Hash: plumbing.NewHash(passthrough.Ref),
	}); err != nil {
		return err
	}

	return nil
}

func handleGitBranch(passthrough Passthrough, dir string, logger GenericLogger) error {
	logger.Debugf("Cloning branch %s to %s\n", passthrough.Ref, dir)

	url := formatGitURI(passthrough.Value, passthrough.Auth)
	opts := git.CloneOptions{
		URL: url,
	}
	if passthrough.Ref != "" {
		opts.ReferenceName = plumbing.ReferenceName(passthrough.Ref)
		opts.Depth = 1
		opts.SingleBranch = true
	}

	if _, err := git.PlainClone(dir, false, &opts); err != nil {
		return err
	}

	return nil
}

// formats a git repository URI by bisecting the git URL,
// infixing the auth string, and ensuring the string ends in `.git`.
// Only `https://` is currently supported
func formatGitURI(url string, auth string) string {
	authSeparator := "@"

	url = strings.TrimPrefix(url, GitProtocol)

	if auth != "" {
		auth = auth + authSeparator
	}

	if !strings.HasSuffix(url, ".git") {
		url = url + ".git"
	}

	return GitProtocol + auth + url
}
