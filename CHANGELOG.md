# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The release dates can be found on the [releases page](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases).

## v3.3.2
### Security

- Upgrade `github.com/go-git/go-git/v5` version to `v5.13.0` to fix ssh vulnerability (!55)

## v3.3.1
### Fixed

- Correctly handle instance URIs beginning with `h`, `t`, `p`, or `s` (!54)

## v3.3.0
### Removed

- Make the following functions private (!51)
  - `CleanPath`
  - `LoadRemote`
  - `ParseRulesetRef`
  - `ProcessPassthroughWithTimeout`
  - `ProcessPassthrough`

## v3.2.1
### Fixed

- Create Target directory with `0700` so `gitlab` user can write to it (!50)

## v3.2.0
### Fixed

- Update `go` version to 1.22 (!47)
- Entire repository referenced by `SAST_RULESET_GIT_REFERENCE` should not be used as ruleset (!47)

## v3.1.1
### Fixed

- Configuration path should not be prepended to the passthrough path (!44)


## v3.1.0
### Added

- Added `SECURE_ENABLE_LOCAL_CONFIGURATION` to allow or disallow local custom ruleset to override remote/default ruleset (!38)

## v3.0.2
### Fixed

- Local custom rulesets passthrough should not have the `value` attribute updated to include the remote download URL (!34)

## v3.0.1
### Fixed

- Clarify error messages when a config file is not found (!35)

## v3.0.0
### Fixed

- Sets the Path to the ruleset in the Config to support remote rulesets (!27)

## v2.0.9
### Fixed

- Clarify error messages for ruleset reference parsing (!28)

## v2.0.8
### Security

- Upgrade `github.com/go-git/go-git/v5` version to `v5.11.0` to fix Denial of Service(DoS) vulnerability (!25)

## v2.0.7
### Added

- Add additional logs to show what's happening as project-level and shared ruleset configs are loaded (!20)

## v2.0.6
### Fixed

- Increase maximum size of a raw or file passthrough to 10MB to accommodate large ruleset files (!24)

## v2.0.5
### Fixed

- Fix incorrect reference to Remote Custom ruleset file (!23)

## v2.0.4
### Added

- Adds support for ambiguous refs to remote branch references (!18)

## v2.0.3
### Fixed

- Fixes incompatibility with latest version of `github.com/otiai10/copy`, upgrade to v1.11.0 (!19)

## v2.0.2
### Fixed

- Fixes remote file passthrough by resolving paths as absolute (!17)

## v2.0.1
### Fixed

- Fixes bug where ConfigFileNotFound should not halt execution (!16)

## v2.0.0
### Added

- Adds LoadRemote function for loading remote rulesets (!15)
- Updates `Load` to handle error handling and unified LoadRelative/LoadRemote behavior (!15)

## v1.4.1
### Fixed

- An error is returned if a request for a URL passthrough is not successful (!10)

## v1.4.0
### Added

- Fixes bug in rules disablement that can occur with `identifier.value` collisions (!8)
- Adds support for overriding rules (!8)


## v1.3.0
### Added

- Rule pack synthesis (!6)
  - Added support for passthrough chains with two modes of operation: append, overwrite
  - Added support for new passthrough types: git, url
  - Added variable interpolation to include environment variables
  - Added timeout setting
  - Added support for filetype validation (xml|json|yaml|toml)

## v1.2.0
### Added

- Exposes `CleanPath()` helper function for lexical cleaning of filepaths (!7)

## v1.1.0
### Added

- Add `ProcessPassthrough()` functionality for generically parsing passthrough rules (!5)

## v1.0.0
### Added

- Add rules package for custom ruleset support (!1)
