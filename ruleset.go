// Package ruleset provides code for the analyzer to use to load external scanner configurations.
// These rulesets are loaded from .gitlab/{sast}-ruleset.toml.
package ruleset

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	toml "github.com/pelletier/go-toml"
	"gopkg.in/yaml.v2"
)

const (
	// EnvVarGitlabFeatures lists Gitlab features available
	EnvVarGitlabFeatures = "GITLAB_FEATURES"
	// GitlabFeatureCustomRulesetsSAST indicates that sast custom rulesets are enabled
	GitlabFeatureCustomRulesetsSAST = "sast_custom_rulesets"
	// PathSAST is the default path to custom sast rules
	PathSAST = ".gitlab/sast-ruleset.toml"
	// PathSecretDetection is the default path to custom secret detection rulesets
	PathSecretDetection = ".gitlab/secret-detection-ruleset.toml" // #nosec
	// GitReferenceSAST indicates a remote ruleset reference is defined
	GitReferenceSAST = "SAST_RULESET_GIT_REFERENCE"
	// GitReferenceSecretDetection indicates a remote ruleset reference is defined
	GitReferenceSecretDetection = "SECRET_DETECTION_RULESET_GIT_REFERENCE"
	// EnableLocalConfiguration indicates to load custom sast rules and instead of default or remote ruleset
	EnableLocalConfiguration = "SECURE_ENABLE_LOCAL_CONFIGURATION"
	// PassthroughFile should be used when the ruleset passthrough is a file.
	PassthroughFile PassthroughType = "file"
	// PassthroughRaw should be used when the ruleset passthrough is defined inline.
	PassthroughRaw PassthroughType = "raw"
	// PassthroughGit should be used when the ruleset is pulled via git pulled via git
	PassthroughGit PassthroughType = "git"
	// PassthroughFileURL should be used to download files
	PassthroughFileURL PassthroughType = "url"

	// ValidatorJSON is used to validate JSON files
	ValidatorJSON Validator = "json"
	// ValidatorTOML is used to validate TOML files
	ValidatorTOML Validator = "toml"
	// ValidatorYAML is used to validate YAML files
	ValidatorYAML Validator = "yaml"
	// ValidatorXML is used to validate XML files
	ValidatorXML Validator = "xml"
	// ValidatorUnknown if validator is not known
	ValidatorUnknown Validator = ""

	// DefaultOverallTimeout should be used to set the overall timeout to evaluate all passthroughs in combination (seconds)
	DefaultOverallTimeout = 60

	// MaxOverallTimeout is the max allowed overall timeout for performing a sequence of passthroughs
	MaxOverallTimeout = 300

	// MaxPassthroughs limits the number of maximally allowed passthroughs (per
	// configuration)
	MaxPassthroughs = 20

	// MaxPassthroughByteSize limits the number of bytes written per (raw|file) passthrough
	MaxPassthroughByteSize = 10000000 // 10MB

	// MaxTargetByteSize limits the size of the target configuration (dir|file) for rule-pack
	// synthesis
	MaxTargetByteSize = 100000000 // 100MB

	// error messages
	errorMessageTimeout                  = "Timeout has to be a non-negative number"
	errorMessageTypeValue                = "Type and/or value field settings are missing"
	errorMessageTargetField              = "No target fields defined"
	errorMessageConfigTargetField        = "No target fields defined in the configuration section"
	errorMessageRelativePath             = "Provided path is not absolute"
	errorMessageInvalidPassthroughMode   = "Invalid passthrough mode; has to be either 'append' or 'overwrite'"
	errorMessageGitOnlySupportsOverwrite = "Passthrough type git only supports mode 'overwrite' at the moment"
	errorMessageMalformedGitURL          = "Malformed Git Url"

	passthroughModeOverwrite = "overwrite"
	passthroughModeAppend    = "append"
)

var errorMessageTooManyPassthroughs = fmt.Sprintf("Number of passthroughs cannot exceed %d", MaxPassthroughs)
var errorMessageTargetDirSizeLimitExceeded = fmt.Sprintf("Target dir size exceeds limit of %d bytes", MaxTargetByteSize)

// PassthroughType determines how the analyzer loads the ruleset which can either be via a
// file or defined inline.
type PassthroughType string

// PassthroughMode determines how the results of passthroughs should be
// applied: append to file or overwrite
type PassthroughMode string

// Validator is used to determine which validator to use to check the
// validatity of a passthrough (intermediate) result
type Validator string

var defaultFilePerm os.FileMode = 0666

// defaultDirectoryPerm needs to be 0700 so that non-root users can create files in this dir
var defaultDirectoryPerm os.FileMode = 0700

func (mode PassthroughMode) append() bool {
	return mode == passthroughModeAppend
}

func (mode PassthroughMode) overwrite() bool {
	return mode == passthroughModeOverwrite
}

// Config is used for overriding default scanner configurations for the analyzers.
type Config struct {
	TargetDir   string        `toml:",omitempty"`
	Description string        `toml:",omitempty"`
	Passthrough []Passthrough `toml:",omitempty"`
	Ruleset     []Ruleset     `toml:",omitempty"`
	Path        string        `toml:",omitempty"`
	Timeout     uint          `toml:",omitempty"`
	Interpolate bool          `toml:",omitempty"`
	Validate    bool          `toml:",omitempty"`
}

// Passthrough is a struct that analyzers use to load external scanner configurations. Users can define
// in a project's ruleset file a PassthroughType (file, raw) and a value. Depending on the type, the value will
// either be a scanner specific file configuration or an inline configuration.
type Passthrough struct {
	Type PassthroughType
	// Target is used for a target file or directory
	Target string
	// subdir cloning for git passthrough
	Subdir string `toml:",omitempty"`
	// (Optional) Authentication
	Auth  string
	Value string
	// examples:
	//  refs/remotes/origin/develop
	//  97f7686db058e2141c0806a477c1e04835c4f395
	Ref       string          `toml:",omitempty"`
	Mode      PassthroughMode `toml:",omitempty"`
	Validator Validator       `toml:",omitempty"`
}

// Identifier is a vulnerability id. Identifier.Value is used to
// filter or override vulnerability information in the final report.
type Identifier struct {
	Type  string
	Value string
}

// Ruleset is used for disabling rules
type Ruleset struct {
	Identifier Identifier
	Disable    bool `toml:",omitempty"`
	Override   Override
}

// Override is used to override rules properties
type Override struct {
	Name        string `toml:"name,omitempty"`
	Message     string `toml:"message,omitempty"`
	Description string `toml:"description,omitempty"`
	Severity    string `toml:"severity,omitempty"`
}

// GenericLogger is a simple logger interface
type GenericLogger interface {
	Info(...interface{})
	Infof(string, ...interface{})
	Warn(...interface{})
	Warnf(string, ...interface{})
	Error(...interface{})
	Errorf(string, ...interface{})
	Debug(...interface{})
	Debugf(string, ...interface{})
	Fatal(...interface{})
	Fatalf(string, ...interface{})
}

func decodeRelaxed(rulesetPath string) (map[string]Config, error) {
	configs := make(map[string]Config)

	b, err := ioutil.ReadFile(rulesetPath)
	if err != nil {
		return nil, err
	}

	if err = toml.Unmarshal(b, &configs); err != nil {
		return nil, &InvalidConfig{
			RulesetPath: rulesetPath,
			Err:         err,
		}
	}

	return configs, nil
}

func decodeStrict(rulesetPath string) (map[string]Config, error) {
	b, err := os.Open(rulesetPath)
	if err != nil {
		return nil, err
	}

	d := toml.NewDecoder(b)
	d.Strict(true)

	configs := make(map[string]Config)
	err = d.Decode(&configs)
	if err != nil {
		return nil, &InvalidConfig{
			RulesetPath: rulesetPath,
			Err:         err,
		}
	}

	return configs, nil
}

// Load accepts a rulesetPath and analyzer. RulesetPath must point to a valid {sast}-ruleset.toml file.
// A single analyzer rule will be returned if one is found.
func Load(rulesetPath string, analyzer string, logger GenericLogger) (*Config, error) {
	var cfg *Config
	var err error

	// TODO: adapt to report-type
	var rulesetGitRef string
	if strings.HasSuffix(rulesetPath, PathSecretDetection) {
		rulesetGitRef = os.Getenv(GitReferenceSecretDetection)
	} else {
		rulesetGitRef = os.Getenv(GitReferenceSAST)
	}

	enableLocalConfig, err := strconv.ParseBool(os.Getenv(EnableLocalConfiguration))
	if err != nil {
		enableLocalConfig = true
	}

	// Load custom config if available
	if enableLocalConfig {
		if _, err = os.Stat(rulesetPath); err == nil {
			logger.Infof("Loading project-level ruleset configuration file from '%s'", rulesetPath)
			cfg, err = LoadRelative(rulesetPath, analyzer)
		} else if os.IsNotExist(err) {
			err = &ConfigFileNotFoundError{RulesetPath: rulesetPath}
		}
	}
	if cfg == nil && rulesetGitRef != "" {
		logger.Info("Loading remote ruleset configuration file")
		cfg, err = loadRemote(rulesetGitRef, analyzer, logger)
	}

	if err != nil {
		switch err.(type) {
		case *NotEnabledError, *ConfigFileNotFoundError, *ConfigNotFoundError:
			logger.Warn(err)
		case *InvalidConfig:
			logger.Fatal(err)
		default:
			return nil, err
		}
	}

	return cfg, nil
}

// LoadRelative accepts a rulesetPath and analyzer. Rulesetpath must point to a valid {sast}-ruleset.toml file.
// A single analyzer rule will be returned if one is found.
func LoadRelative(rulesetPath string, analyzer string) (*Config, error) {
	if !customRulesetEnabled() {
		return nil, &NotEnabledError{}
	}

	return buildConfig(rulesetPath, analyzer, "")
}

// loadRemote accepts a rulesetRef string and analyzer.
// rulesetRef must point to an accessible remote repository to be cloned and have its ruleset path evaluated.
// The remote repository must contain the default ruleset path with a valid {sast}-ruleset.toml file.
// A single analyzer rule will be returned if one is found.
func loadRemote(rulesetRef string, analyzer string, logger GenericLogger) (*Config, error) {
	if !customRulesetEnabled() {
		return nil, &NotEnabledError{}
	}

	pt, err := parseRulesetRef(rulesetRef)
	if err != nil {
		return nil, err
	}

	tmpDir, err := os.MkdirTemp("/tmp", "glsastrulesetremoteref")
	if err != nil {
		return nil, err
	}
	pt.Target = tmpDir

	gitSource, err := cloneGit(*pt, logger)
	if err != nil {
		return nil, err
	}

	rulesetFile := PathSAST
	if analyzer == "secrets" {
		rulesetFile = PathSecretDetection
	}

	rulesetPath := filepath.Join(gitSource, rulesetFile)

	return buildConfig(rulesetPath, analyzer, gitSource)
}

func buildConfig(rulesetPath string, analyzer string, sourceDir string) (*Config, error) {
	if _, err := os.Stat(rulesetPath); err != nil && os.IsNotExist(err) {
		return nil, &ConfigFileNotFoundError{
			RulesetPath: rulesetPath,
		}
	}

	configs, err := decodeRelaxed(rulesetPath)
	if err != nil {
		return nil, err
	}

	interpolate(configs)
	setSensibleDefaults(configs, rulesetPath, sourceDir)

	err = validateConfig(configs)
	if err != nil {
		return nil, err
	}

	if config, ok := configs[analyzer]; ok {
		return &config, nil
	}

	return nil, &ConfigNotFoundError{
		Analyzer:    analyzer,
		RulesetPath: rulesetPath,
	}
}

func validatorForFile(path string) Validator {
	var extension = filepath.Ext(path)
	switch strings.ToLower(extension) {
	case ".xml":
		return ValidatorXML
	case ".yaml", ".yml":
		return ValidatorYAML
	case ".toml":
		return ValidatorTOML
	case ".json":
		return ValidatorJSON
	}
	return ValidatorUnknown
}

func relativePath(config *Config) bool {
	return config.TargetDir != "" && !filepath.IsAbs(config.TargetDir)
}

func invalidTimeout(config *Config) bool {
	return config.Timeout < 0 || config.Timeout > MaxOverallTimeout
}

func emptyConfigTargetDirForPassthroughs(config *Config) bool {
	if len(config.Passthrough) > 1 {
		// passthrough sequence
		if config.TargetDir == "" {
			return true
		}
	}
	return false
}

func tooManyPassthroughs(config *Config) bool {
	return len(config.Passthrough) > MaxPassthroughs
}

func invalidMode(pt *Passthrough) bool {
	return pt.Mode != passthroughModeAppend && pt.Mode != passthroughModeOverwrite
}

func missingTypeValue(pt *Passthrough) bool {
	return pt.Type == "" || pt.Value == ""
}

func noAppendForGit(pt *Passthrough) bool {
	return pt.Type == PassthroughGit && pt.Mode == passthroughModeAppend
}

func emptyTargetForRawPassthroughs(config *Config, pt *Passthrough) bool {
	if len(config.Passthrough) > 1 {
		// this is part of a passthrough sequence
		if pt.Target == "" {
			return true
		}
	}
	return false
}

func emptyTargetForSingleGitPassthrough(config *Config, _ *Passthrough) bool {
	return config.TargetDir == "" && len(config.Passthrough) == 1 && config.Passthrough[0].Type == PassthroughGit
}

func validateConfig(configs map[string]Config) error {
	var sb strings.Builder

	// config validity checks and default settings
	for name, config := range configs {
		// validate config parameters
		if relativePath(&config) {
			sb.WriteString(fmt.Sprintf("%s\n", errorMessageRelativePath))
		}
		if invalidTimeout(&config) {
			sb.WriteString(fmt.Sprintf("%s\n", errorMessageTimeout))
		}
		if emptyConfigTargetDirForPassthroughs(&config) {
			sb.WriteString(fmt.Sprintf("%s '%s'\n", errorMessageConfigTargetField, name))
		}
		if tooManyPassthroughs(&config) {
			sb.WriteString(fmt.Sprintf("%s '%s'\n", errorMessageTooManyPassthroughs, name))
		}

		// validate passthroughs
		for i := range config.Passthrough {
			pt := &config.Passthrough[i]
			if invalidMode(pt) {
				sb.WriteString(fmt.Sprintf("%s '%s'\n", errorMessageInvalidPassthroughMode, name))
			}
			if missingTypeValue(pt) {
				sb.WriteString(fmt.Sprintf("%s '%s'\n", errorMessageTypeValue, name))
			}
			if noAppendForGit(pt) {
				sb.WriteString(fmt.Sprintf("%s '%s'\n", errorMessageGitOnlySupportsOverwrite, name))
			}
			if emptyTargetForRawPassthroughs(&config, pt) {
				sb.WriteString(fmt.Sprintf("%s '%s'\n", errorMessageTargetField, name))
			}
			if emptyTargetForSingleGitPassthrough(&config, pt) {
				sb.WriteString(fmt.Sprintf("%s '%s'\n", errorMessageTargetField, name))
			}
		}
	}

	if sb.Len() > 0 {
		return errors.New(sb.String())
	}
	return nil
}

// interpolate is used to expand env variables as they are used in the
// configuration file
func interpolate(configs map[string]Config) {
	for name, config := range configs {
		if !config.Interpolate {
			continue
		}
		cfg := configs[name]
		cfg.TargetDir = os.ExpandEnv(cfg.TargetDir)
		cfg.Path = os.ExpandEnv(cfg.Path)

		for i := range cfg.Passthrough {
			pt := &config.Passthrough[i]
			pt.Ref = os.ExpandEnv(pt.Ref)
			pt.Target = os.ExpandEnv(pt.Target)
			pt.Value = os.ExpandEnv(pt.Value)
		}

		configs[name] = cfg
	}
}

func setSensibleDefaults(configs map[string]Config, rulesetPath string, sourceDir string) {
	// config validity checks and default settings
	for i, config := range configs {
		if rulesetPath != "" {
			config.Path = rulesetPath
		}

		if config.TargetDir == "" && len(config.Passthrough) > 1 ||
			(config.TargetDir == "" && len(config.Passthrough) == 1 && config.Passthrough[0].Type == PassthroughGit) {
			// target is only relevant if we have more than a single
			// passthrough
			config.TargetDir = fmt.Sprintf("/tmp/ruledir%d", time.Now().UnixNano())
			configs[i] = config
		}
		if config.Timeout == 0 {
			config.Timeout = uint(DefaultOverallTimeout)
			configs[i] = config
		}
		for i := range config.Passthrough {
			pt := &config.Passthrough[i]
			// set override as default
			if pt.Mode == "" {
				pt.Mode = passthroughModeOverwrite
			}

			// propagate the top-level configuration directory as prefix to the
			// passthrough configurations
			switch pt.Type {
			case PassthroughGit:
				pt.Target = filepath.Join(config.TargetDir, cleanPath(pt.Target))
			case PassthroughRaw, PassthroughFileURL:
				if pt.Target == "" {
					pt.Target = fmt.Sprintf("rulefile%d", time.Now().UnixNano())
				}
				pt.Target = filepath.Join(config.TargetDir, cleanPath(pt.Target))
			case PassthroughFile:
				if pt.Target == "" {
					pt.Target = pt.Value
				}
				pt.Value = filepath.Join(sourceDir, cleanPath(pt.Value))
				pt.Target = filepath.Join(config.TargetDir, cleanPath(pt.Target))
			}

			// automatically infer the validator based on the file extension
			if config.Validate && pt.Validator == "" {
				pt.Validator = validatorForFile(pt.Target)
			}
		}
	}
}

// targetSize computes the overall file/directory size
func targetSize(path string) (int64, error) {
	abs, err := filepath.Abs(path)
	if err != nil {
		return -1, err
	}
	var size int64
	err = filepath.Walk(abs, func(absolutePath string, info os.FileInfo, err error) error {
		// return start path itself
		if absolutePath == abs {
			return nil
		}
		if err != nil {
			return err
		}
		if !info.IsDir() {
			size += info.Size()
		} else {
			n, err := targetSize(filepath.Join(absolutePath, info.Name()))
			if err != nil {
				return err
			}
			size += n
		}
		return err
	})
	return size, err
}

// ProcessPassthroughs processes multiple rulesets and returns the combined results
func ProcessPassthroughs(config *Config, logger GenericLogger) (string, error) {
	ctx := context.Background()
	ctxWithTimeout, cancelFn := context.WithTimeout(ctx, time.Duration(config.Timeout)*time.Second)
	defer cancelFn()

	logger.Infof("Running passthroughs with timeout %d", config.Timeout)

	var err error
	var path string

	// target dir can be empty if we only have a single passthrough
	if config.TargetDir != "" {
		// Make sure that target dir exist before we start
		err = os.MkdirAll(config.TargetDir, defaultDirectoryPerm)
		if err != nil {
			return "", err
		}
	}

	for _, pt := range config.Passthrough {
		path, err = processPassthroughWithTimeout(ctxWithTimeout, pt, logger)
		if err != nil {
			break
		}

		// continuously check the overall size of the target configuration after
		// the application of a single passthrough
		if config.TargetDir != "" {
			size, err := targetSize(config.TargetDir)
			if err != nil {
				break
			}
			if size > MaxTargetByteSize {
				return config.TargetDir, errors.New(errorMessageTargetDirSizeLimitExceeded)
			}
		}

		// backwards-compatible behavior: handles a single passthrough
		if len(config.Passthrough) == 1 {
			size, err := targetSize(path)
			if err != nil {
				break
			}
			if size > MaxTargetByteSize {
				return config.TargetDir, errors.New(errorMessageTargetDirSizeLimitExceeded)

			}
			return path, err
		}
	}

	return config.TargetDir, err
}

// processPassthroughWithTimeout runs the ProcessPassthrough functions in a
// timeout context
func processPassthroughWithTimeout(ctx context.Context, passthrough Passthrough, logger GenericLogger) (string, error) {
	result := make(chan struct {
		path string
		err  error
	}, 1)
	go func() {
		r, err := processPassthrough(passthrough, logger)
		result <- struct {
			path string
			err  error
		}{r, err}
		close(result)
	}()

	for {
		select {
		case r := <-result:
			return r.path, r.err
		case <-ctx.Done():
			return "", ctx.Err()
		}
	}
}

// validateFormat can be used to validateFormat the intermedate results for passthroughs
func validateFormat(passthrough Passthrough, logger GenericLogger) error {
	file, err := os.Open(passthrough.Target)
	if err != nil {
		panic(err)
	}

	defer closeDescriptor(file, logger)

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	switch passthrough.Validator {
	case ValidatorJSON:
		err = validateJSON(data)
	case ValidatorTOML:
		err = validateTOML(data)
	case ValidatorYAML:
		err = validateYAML(data)
	case ValidatorXML:
		err = validateXML(data)
	case ValidatorUnknown:
		err = nil
	default:
		err = fmt.Errorf("Unsupported validator %s", passthrough.Validator)
	}

	if err != nil {
		return fmt.Errorf("Validation error for %s [%s]: %s", passthrough.Target, passthrough.Validator, err.Error())
	}
	logger.Debugf("Validation ok for %s [%s]", passthrough.Target, passthrough.Validator)

	return err
}

func validateJSON(data []byte) error {
	return json.Unmarshal(data, new(interface{}))
}

func validateTOML(data []byte) error {
	return toml.Unmarshal(data, new(interface{}))
}

func validateYAML(data []byte) error {
	return yaml.Unmarshal(data, new(interface{}))
}

func validateXML(data []byte) error {
	return xml.Unmarshal(data, new(interface{}))
}

// processPassthrough leverages a pre-existing file (file), mints a new file
// (raw), downloads a file (url) or clones a git repository (git) and returns
// the path to the configuration file or directory
func processPassthrough(passthrough Passthrough, logger GenericLogger) (string, error) {
	var path string
	var err error

	switch passthrough.Type {
	case PassthroughFile:
		path, err = passthroughFile(passthrough, logger)
	case PassthroughRaw:
		path, err = passthroughRaw(passthrough, logger)
	case PassthroughFileURL:
		path, err = passthroughFileURL(passthrough, logger)
	case PassthroughGit:
		path, err = passthroughGit(passthrough, logger)
	default:
		path = ""
		err = fmt.Errorf("Unsupported passthrough type: %s", passthrough.Type)
	}

	return path, err
}

// DisabledIdentifiers uses the config pre-loaded by the analyzer then
// constructs a list of identifiers that will be ignored when reporting vulnerabilities
func (config *Config) DisabledIdentifiers() (map[string]bool, error) {
	disabledIdentifiers := make(map[string]bool)
	for _, ruleset := range config.Ruleset {
		if ruleset.Disable {
			key := strings.Join([]string{ruleset.Identifier.Type, ruleset.Identifier.Value}, "-")
			disabledIdentifiers[key] = true
		}
	}

	return disabledIdentifiers, nil
}

// IdentifiersWithOverrides uses the config pre-loaded by the analyzer then
// constructs a list of identifiers that will be overridden when reporting vulnerabilities
func (config *Config) IdentifiersWithOverrides() (map[string]Ruleset, error) {
	identifiersWithOverrides := make(map[string]Ruleset)
	for _, ruleset := range config.Ruleset {
		if (ruleset.Override != Override{}) {
			key := strings.Join([]string{ruleset.Identifier.Type, ruleset.Identifier.Value}, "-")
			identifiersWithOverrides[key] = ruleset
		}
	}

	return identifiersWithOverrides, nil
}

type descriptor interface {
	Close() error
}

func closeDescriptor(fd descriptor, logger GenericLogger) {
	if err := fd.Close(); err != nil {
		logger.Error(err.Error())
	}
}

func (passthrough *Passthrough) writePassthroughToTarget(reader io.Reader, logger GenericLogger) (string, error) {
	buf := new(bytes.Buffer)
	noOfBytes, err := buf.ReadFrom(reader)
	if err != nil {
		return "", err
	}
	if noOfBytes > MaxPassthroughByteSize {
		return "", fmt.Errorf("Passthrough size should not exceed %d bytes", MaxPassthroughByteSize)
	}

	pathToDst := path.Dir(passthrough.Target)

	if _, err := os.Stat(pathToDst); os.IsNotExist(err) {
		err = os.MkdirAll(pathToDst, defaultDirectoryPerm)
		if err != nil {
			return "", err
		}
	}

	flags := os.O_RDWR | os.O_CREATE
	if passthrough.Mode.append() {
		flags = flags | os.O_APPEND
	} else {
		flags = flags | os.O_TRUNC
	}

	tmpConfig, err := os.OpenFile(passthrough.Target, flags, defaultFilePerm)
	if err != nil {
		return "", err
	}
	defer closeDescriptor(tmpConfig, logger)

	logger.Debugf("Write to %s [mode: %s]", tmpConfig.Name(), passthrough.Mode)
	_, err = io.Copy(tmpConfig, buf)
	if err != nil {
		return "", err
	}

	// if a validator is set, we validate the target file after
	// every modification
	if passthrough.Validator != "" {
		err = validateFormat(*passthrough, logger)
	}

	return tmpConfig.Name(), err
}

// passthroughFileURL downloads a file from a given url
func passthroughFileURL(passthrough Passthrough, logger GenericLogger) (string, error) {
	resp, err := http.Get(passthrough.Value)
	if err != nil {
		return "", fmt.Errorf("get URL passthrough: %w", err)
	}
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("requested URL %q returned a non-200 response status (%d)", passthrough.Value, resp.StatusCode)
	}

	defer closeDescriptor(resp.Body, logger)

	return passthrough.writePassthroughToTarget(resp.Body, logger)
}

// passthroughFile uses a file that is present in the local git tree
func passthroughFile(passthrough Passthrough, logger GenericLogger) (string, error) {
	logger.Debugf("Using config from custom ruleset passthrough file")

	if passthrough.Value != passthrough.Target {
		data, err := os.Open(passthrough.Value)
		if err != nil {
			return "", fmt.Errorf("read file passthrough: %w", err)
		}

		defer closeDescriptor(data, logger)

		return passthrough.writePassthroughToTarget(data, logger)
	}

	configPath := cleanPath(passthrough.Value)
	return configPath, nil
}

// passthroughGit clones a remote git repository and returns the target path
func passthroughGit(passthrough Passthrough, logger GenericLogger) (string, error) {
	return cloneGit(passthrough, logger)
}

// passthroughRaw writes raw configuration to a file
func passthroughRaw(passthrough Passthrough, logger GenericLogger) (string, error) {
	logger.Debugf("Loading config from custom ruleset via raw passthrough")

	reader := strings.NewReader(passthrough.Value)
	return passthrough.writePassthroughToTarget(reader, logger)
}

// cleanPath makes a path safe for use with filepath.Join. This is done by not
// only cleaning the path, but also (if the path is relative) adding a leading
// '/' and cleaning it (then removing the leading '/'). This ensures that a
// path resulting from prepending another path will always resolve to lexically
// be a subdirectory of the prefixed path. This is all done lexically, so paths
// that include symlinks won't be safe as a result of using cleanPath.
//
// This function comes from runC (libcontainer/utils/utils.go):
// https://github.com/opencontainers/runc/blob/d636ad6256f9194b0f4c6ee181e75fb36e3446d8/libcontainer/utils/utils.go#L53
func cleanPath(path string) string {
	// Deal with empty strings nicely.
	if path == "" {
		return ""
	}

	// Ensure that all paths are cleaned (especially problematic ones like
	// "/../../../../../" which can cause lots of issues).
	path = filepath.Clean(path)

	// If the path isn't absolute, we need to do more processing to fix paths
	// such as "../../../../<etc>/some/path". We also shouldn't convert absolute
	// paths to relative ones.
	if !filepath.IsAbs(path) {
		path = filepath.Clean(string(os.PathSeparator) + path)
		// This can't fail, as (by definition) all paths are relative to root.
		path, _ = filepath.Rel(string(os.PathSeparator), path)
	}

	// Clean the path again for good measure.
	return filepath.Clean(path)
}

// customRulesetEnabled checks if custom rulesets are enabled.
func customRulesetEnabled() bool {
	if features := os.Getenv(EnvVarGitlabFeatures); !strings.Contains(features, GitlabFeatureCustomRulesetsSAST) {
		return false
	}
	return true
}
