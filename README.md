# Analyzers Ruleset Library
[![](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/commits/master)
[![](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/jobs)

This repository contains Go modules for implementing customization of rulesets for GitLab Secure analyzers.

### Testing

```sh
go test ./...
```

Certain unit tests require additional credentials in order to test authenticated fetching
of private repositories. By default the unit tests will assert an "authentication required"
error but fetching can be enabled by passing in the necessary credentials:

```sh
GITLAB_AUTH="theoretick:glpat-notarealtoken" go test ./...
```

## Support

This is an internal package used for developing GitLab-specific features. This package is not meant for public consumption, and breaking changes might be introduced at any time. See [publicly available internal tooling](https://about.gitlab.com/support/statement-of-support#publicly-available-internal-tooling) for more details.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
