package ruleset

import (
	"bufio"
	"bytes"
	"crypto/rand"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"
	"testing"

	git "github.com/go-git/go-git/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// Logger is a placeholder interface for a logger
type Logger struct{}

func (l Logger) Info(...interface{})           {}
func (l Logger) Infof(string, ...interface{})  {}
func (l Logger) Warn(...interface{})           {}
func (l Logger) Warnf(string, ...interface{})  {}
func (l Logger) Error(...interface{})          {}
func (l Logger) Errorf(string, ...interface{}) {}
func (l Logger) Debug(...interface{})          {}
func (l Logger) Debugf(string, ...interface{}) {}
func (l Logger) Fatal(...interface{})          {}
func (l Logger) Fatalf(string, ...interface{}) {}

// setFeatureEnv is a helper to explicitly set the value feature ENV
func setFeatureEnv(value string) (string, error) {
	err := os.Setenv(EnvVarGitlabFeatures, value)
	if err != nil {
		return value, err
	}
	return value, nil
}

func errEqual(t *testing.T, got error, want error) {
	if got != nil && want != nil {
		// support comparing errors without unwrap
		if got.Error() == want.Error() {
			return
		}
		t.Fatalf("expected error: %v \ngot: %v", want, got)
	} else if got != nil {
		t.Fatal(got)
	}
}

func randomFileWithSize(size int) (string, error) {
	bufSize := 1024
	path := fmt.Sprintf("/tmp/random%d", os.Getpid())

	f, err := os.Create(path)
	defer f.Close()

	if err != nil {
		return "", err
	}

	fb := bufio.NewWriter(f)
	defer fb.Flush()

	data := make([]byte, bufSize)
	for i := size; i > 0; i -= bufSize {
		if _, err = rand.Read(data); err != nil {
			break
		}
		bytereader := bytes.NewReader(data)
		if _, err = io.Copy(fb, bytereader); err != nil {
			break
		}
	}

	return path, err
}

const (
	// https://gitlab.com/gitlab-org/security-products/tests/go/-/blob/Custom-Ruleset-FREEZE/.gitlab/sast-ruleset.toml
	customRulesetRef = "gitlab.com/gitlab-org/security-products/tests/go@fa066953178503705adaf9311362becba5df1b0e"

	// https://gitlab.com/gitlab-org/security-products/tests/go/-/blob/Custom-Ruleset-Raw-FREEZE/.gitlab/sast-ruleset.toml
	customRulesetRawRef = "gitlab.com/gitlab-org/security-products/tests/go@2488eaa671cc02ecc73cddffc5a2b4c528469933"

	// https://gitlab.com/gitlab-org/security-products/tests/go/-/blob/Custom-Ruleset-Synthesis-FREEZE/.gitlab/sast-ruleset.toml
	customRulesetSynthesisRef = "gitlab.com/gitlab-org/security-products/tests/go@d832d7323172e0ddf740a739520ff3d92ae0d471"
)

func TestGitPassthroughs(t *testing.T) {
	tmpdir, err := ioutil.TempDir("/tmp", "test")
	require.NoError(t, err)
	defer os.RemoveAll(tmpdir)

	expectedSha := "97f7686db058e2141c0806a477c1e04835c4f395"
	expectedBranch := "9e5ea7819835d3fb611aa2fd44675d95677f5a59"
	branchSubdir := "branch"
	shaSubdir := "sha"

	cfg := Config{
		TargetDir: tmpdir,
		Timeout:   100,
		Passthrough: []Passthrough{
			{
				Type:   PassthroughGit,
				Value:  "https://gitlab.com/gitlab-org/security-products/sast-rules",
				Ref:    expectedSha,
				Target: shaSubdir,
			},
			{
				Type:   PassthroughGit,
				Value:  "https://gitlab.com/gitlab-org/security-products/sast-rules",
				Ref:    "refs/heads/lcong",
				Target: branchSubdir,
			},
		},
	}
	gittest := map[string]Config{
		"gittest": cfg,
	}
	setSensibleDefaults(gittest, "", "")

	err = handleGitSha(cfg.Passthrough[0], cfg.Passthrough[0].Target, Logger{})
	if err != nil {
		t.Errorf("Error while running Git passthroughs: %s", err.Error())
	}
	sharepo, err := git.PlainOpen(filepath.Join(tmpdir, shaSubdir))
	if err != nil {
		t.Errorf("Error trying to open git repository: %s", err.Error())
	}
	sha, err := sharepo.Head()
	if err != nil {
		t.Errorf("Error trying to retrieve HEAD: %s", err.Error())
	}
	if sha.Hash().String() != expectedSha {
		t.Errorf("expected git hash to be %s but got %s", expectedSha, sha.Hash().String())
	}

	err = handleGitBranch(cfg.Passthrough[1], cfg.Passthrough[1].Target, Logger{})
	if err != nil {
		t.Errorf("Error while running Git passthroughs: %s", err.Error())
	}
	branchrepo, err := git.PlainOpen(filepath.Join(tmpdir, branchSubdir))
	if err != nil {
		t.Errorf("Error trying to open git repository: %s", err.Error())
	}
	branch, err := branchrepo.Head()
	if err != nil {
		t.Errorf("Error trying to retrieve HEAD: %s", err.Error())
	}
	if branch.Hash().String() != expectedBranch {
		t.Errorf("expected git hash to be %s but got %s", expectedBranch, branch.Hash().String())
	}

	// cleaning up tmpdir
	os.RemoveAll(filepath.Join(tmpdir, branchSubdir))
	os.RemoveAll(filepath.Join(tmpdir, shaSubdir))

	// now we execute the passthroughs normally and make sure that all git
	// information is removed from the repository
	_, err = ProcessPassthroughs(&cfg, Logger{})
	if err != nil {
		t.Errorf("something went wrong trying to process passthroughs")
	}
	_, err = git.PlainOpen(filepath.Join(tmpdir, shaSubdir))
	if err == nil {
		t.Errorf("Git config not removed")
	}
}

func TestTimeoutExceeded(t *testing.T) {
	cfg := Config{
		Timeout: 0,
		Passthrough: []Passthrough{
			{
				Type:      PassthroughFileURL,
				Value:     "https://semgrep.dev/c/p/gosec",
				Target:    "gosec.yml",
				Validator: ValidatorYAML,
			},
		},
	}
	_, err := ProcessPassthroughs(&cfg, Logger{})
	if err == nil {
		t.Errorf("timeout error expected but got none")
	}
}

func TestMaxPassthroughsExceeded(t *testing.T) {
	cfg := Config{
		Passthrough: []Passthrough{},
	}
	passthrough := Passthrough{
		Type:   PassthroughRaw,
		Value:  "x",
		Target: "gosec.yml",
	}
	for i := 0; i <= MaxPassthroughs+1; i++ {
		cfg.Passthrough = append(cfg.Passthrough, passthrough)
	}
	namedCfg := map[string]Config{
		"named": cfg,
	}
	setSensibleDefaults(namedCfg, "", "")
	err := validateConfig(namedCfg)
	if err == nil {
		t.Errorf("passthrough error expected but got none")
	}
}

func TestPassthroughLimitsExceeded(t *testing.T) {
	path, err := randomFileWithSize(MaxPassthroughByteSize)
	cfg := Config{
		Timeout:   100,
		TargetDir: fmt.Sprintf("%s_%s", path, "out"),
		Passthrough: []Passthrough{
			{
				Type:  PassthroughFile,
				Value: path,
			},
		},
	}
	namedCfg := map[string]Config{
		"named": cfg,
	}
	setSensibleDefaults(namedCfg, "", "")
	err = validateConfig(namedCfg)
	_, err = ProcessPassthroughs(&cfg, Logger{})
	if err == nil {
		t.Errorf("passthrough error expected but got none")
	}
}

func TestOverallSizeLimitsExceeded(t *testing.T) {
	path1, err := randomFileWithSize(MaxTargetByteSize / 2)
	path2, err := randomFileWithSize(MaxTargetByteSize / 2)
	cfg := Config{
		Timeout:   100,
		TargetDir: fmt.Sprintf("%s_%s", path1, "out"),
		Passthrough: []Passthrough{
			{
				Type:  PassthroughFile,
				Value: path1,
			},
			{
				Type:  PassthroughFile,
				Value: path2,
			},
		},
	}
	namedCfg := map[string]Config{
		"named": cfg,
	}
	setSensibleDefaults(namedCfg, "", "")
	err = validateConfig(namedCfg)
	_, err = ProcessPassthroughs(&cfg, Logger{})
	if err == nil {
		t.Errorf("passthrough error expected but got none")
	}
}

func TestValidate(t *testing.T) {
	tests := []struct {
		name, contents, wantErr string
		validator               Validator
	}{
		{
			name:      "valid XML",
			validator: ValidatorXML,
			contents:  `<definitions><entry1 name="test"/><entry2 name="another test"/></definitions>`,
		},
		{
			name:      "valid JSON",
			validator: ValidatorJSON,
			contents:  `{ "valid": "json" }`,
		},
		{
			name:      "valid TOML",
			validator: ValidatorTOML,
			contents: `[test]
                   [[gosec.passthrough]]
                     type  = "file"
                     target = ""
                     value = "gosec-sast-config.json"`,
		},
		{
			name:      "valid YAML",
			contents:  `include: '.gitlab-ci.yml'`,
			validator: ValidatorYAML,
		},
		{
			name:      "invalid XML",
			wantErr:   "XML syntax error on line 1: attribute name without = in element",
			validator: ValidatorXML,
			contents:  `<definitions><entry1 name"test"/><entry2 name"another test"/><definitions>`,
		},
		{
			name:      "invalid JSON",
			wantErr:   "unexpected end of JSON input",
			validator: ValidatorJSON,
			contents:  `{"}`,
		},
		{
			name:      "invalid TOML",
			wantErr:   `was expecting token =, but got "invalid" instead`,
			validator: ValidatorTOML,
			contents:  `contains invalid toml`,
		},
		{
			name:      "invalid YAML",
			wantErr:   `yaml: did not find expected alphabetic or numeric character`,
			validator: ValidatorYAML,
			contents:  `* include: '.gitlab-ci.yml'`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			config := Config{
				Timeout:     60,
				Interpolate: true,
				Passthrough: []Passthrough{
					{
						Validator: tt.validator,
						Type:      PassthroughRaw,
						Value:     tt.contents,
						Target:    filepath.Join(t.TempDir(), "some-file"),
					},
				},
			}
			configMap := map[string]Config{
				tt.name: config,
			}

			setSensibleDefaults(configMap, "", "")
			_, err := ProcessPassthroughs(&config, Logger{})
			if tt.wantErr != "" {
				require.ErrorContains(t, err, tt.wantErr)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestInterpolate(t *testing.T) {
	varname := "TEST"

	os.Setenv(varname, "interpolate")

	got := map[string]Config{
		"name": {
			TargetDir:   "$" + varname,
			Timeout:     60,
			Interpolate: true,
			Passthrough: []Passthrough{
				{
					Type:   PassthroughRaw,
					Value:  "$" + varname,
					Target: "$" + varname,
				},
			},
		},
	}

	want := map[string]Config{
		"name": {
			TargetDir: "interpolate",
			Passthrough: []Passthrough{
				{Type: "raw", Target: "interpolate", Value: "interpolate"},
			},
			Timeout:     60,
			Interpolate: true,
		},
	}

	interpolate(got)
	require.Equal(t, want, got)
}

func TestProcessPassthroughs(t *testing.T) {
	tests := []struct {
		Description                               string
		Config                                    Config
		ExpectedCreatedFilenames                  []string
		ExpectedReturnedPathExcludingTmpDirectory string
	}{
		{
			Description: "Multiple Passthroughs should return a directory path",
			Config: Config{
				Timeout: 60,
				Passthrough: []Passthrough{
					{
						Type:   PassthroughRaw,
						Value:  "semgrep rule",
						Target: "bar.yml",
					},
					{
						Type:   PassthroughFile,
						Value:  "testdata/gosec-config.json",
						Target: "foo.json",
					},
					{
						Type:      PassthroughFileURL,
						Value:     "https://semgrep.dev/c/p/gosec",
						Target:    "gosec.yml",
						Validator: ValidatorYAML,
					},
				},
			},
			ExpectedCreatedFilenames:                  []string{"bar.yml", "foo.json", "gosec.yml"},
			ExpectedReturnedPathExcludingTmpDirectory: "",
		},
		{
			Description: "A Single Passthrough should return a filepath",
			Config: Config{
				Timeout: 60,
				Passthrough: []Passthrough{
					{
						Type:   PassthroughFile,
						Value:  "testdata/gosec-config.json",
						Target: "foo.json",
					},
				},
			},
			ExpectedCreatedFilenames:                  []string{"foo.json"},
			ExpectedReturnedPathExcludingTmpDirectory: "foo.json",
		},
		{
			Description: "Single passthrough with path",
			Config: Config{
				TargetDir: "",
				Timeout:   60,
				Path:      "/app/.gitlab/sast-ruleset.toml",
				Passthrough: []Passthrough{
					{
						Type:   PassthroughFile,
						Value:  "testdata/python.yml",
						Target: "python.yml",
						Mode:   passthroughModeOverwrite,
					},
				},
			},
			ExpectedCreatedFilenames:                  []string{"python.yml"},
			ExpectedReturnedPathExcludingTmpDirectory: "python.yml",
		},
	}

	for _, tt := range tests {
		t.Run(tt.Description, func(t *testing.T) {
			tmpdir, err := os.MkdirTemp("/tmp", "test")
			require.NoError(t, err)
			defer os.Remove(tmpdir)
			tt.Config.TargetDir = tmpdir

			key := strings.Join(tt.ExpectedCreatedFilenames, "|")
			cfg := map[string]Config{
				key: tt.Config,
			}

			setSensibleDefaults(cfg, "", "")
			path, err := ProcessPassthroughs(&tt.Config, Logger{})
			require.NoError(t, err)

			require.Equal(t, filepath.Join(tmpdir, tt.ExpectedReturnedPathExcludingTmpDirectory), path)

			fileInfo, err := ioutil.ReadDir(tmpdir)
			if err != nil {
				t.Fatal(err.Error())
			}

			var filenames []string
			for _, f := range fileInfo {
				filenames = append(filenames, f.Name())
			}
			require.Equal(t, tt.ExpectedCreatedFilenames, filenames)
		})
	}
}

func TestProcessPassthroughModes(t *testing.T) {
	tmpdir, err := ioutil.TempDir("/tmp", "test")
	require.NoError(t, err)
	defer os.Remove(tmpdir)

	cfg := map[string]Config{
		`12{
  "G104": {
    "html/template.Template": [
      "Execute"
    ]
  }
}`: {
			TargetDir: tmpdir,
			Timeout:   60,
			Passthrough: []Passthrough{
				{
					Type:   PassthroughRaw,
					Value:  "1",
					Target: "bar.yml",
				},
				{
					Type:   PassthroughRaw,
					Value:  "2",
					Target: "bar.yml",
					Mode:   "append",
				},
				{
					Type:   PassthroughFile,
					Value:  "testdata/gosec-config.json",
					Target: "bar.yml",
					Mode:   "append",
				},
			},
		},
		"overwrite": {
			TargetDir: tmpdir,
			Timeout:   60,
			Passthrough: []Passthrough{
				{
					Type:   PassthroughRaw,
					Value:  "1",
					Target: "bar.yml",
				},
				{
					Type:   PassthroughRaw,
					Value:  "2",
					Target: "bar.yml",
					Mode:   "append",
				},
				{
					Type:   PassthroughFile,
					Value:  "testdata/gosec-config.json",
					Target: "bar.yml",
					Mode:   "append",
				},
				{
					Type:   PassthroughRaw,
					Value:  "overwrite",
					Target: "bar.yml",
					Mode:   "overwrite",
				},
			},
		},
	}

	setSensibleDefaults(cfg, "", "")
	for expect, config := range cfg {
		_, err := ProcessPassthroughs(&config, Logger{})
		require.NoError(t, err)

		content, err := ioutil.ReadFile(filepath.Join(tmpdir, "bar.yml"))
		require.NoError(t, err)
		require.Equal(t, expect, string(content))
	}
}

func TestInvalidConfiguration(t *testing.T) {
	var configs = []map[string]Config{
		{
			errorMessageTimeout: Config{
				TargetDir: "/sgrules",
				Timeout:   100000,
				Passthrough: []Passthrough{
					{
						Type:  PassthroughGit,
						Value: "https://gitlab.com/semgrep-rules",
					},
				},
			},
		},
		{
			errorMessageTypeValue: Config{
				TargetDir: "/sgrules",
				Passthrough: []Passthrough{
					{
						Value: "https://gitlab.com/semgrep-rules",
						Ref:   "test",
					},
				},
			},
		},
		{
			errorMessageTypeValue: Config{
				TargetDir: "/sgrules",
				Passthrough: []Passthrough{
					{
						Type: PassthroughGit,
						Ref:  "test",
					},
				},
			},
		},
		{
			errorMessageRelativePath: Config{
				TargetDir: "../sgrules",
				Passthrough: []Passthrough{
					{
						Type: PassthroughGit,
						Ref:  "test",
					},
				},
			},
		},
		{
			errorMessageGitOnlySupportsOverwrite: Config{
				TargetDir: "/sgrules",
				Passthrough: []Passthrough{
					{
						Type:  PassthroughGit,
						Value: "https://gitlab.com/semgrep-rules",
						Ref:   "test",
						Mode:  passthroughModeAppend,
					},
				},
			},
		},
	}

	for _, entry := range configs {
		setSensibleDefaults(entry, "", "")
		err := validateConfig(entry)
		if err == nil {
			t.Fatalf("Unreported error")
		}
		for wantedMessage := range entry {
			if !strings.HasPrefix(err.Error(), wantedMessage) {
				t.Errorf("Wrong result. Expecting:\n%v\nGot:\n%v", wantedMessage, err.Error())
			}
		}
	}
}

func TestLoad(t *testing.T) {
	var tests = []struct {
		enableCustomRulesets bool
		description          string
		rulePath             string
		ruleRef              string
		enableLocalConfig    string
		analyzer             string
		wantRule             *Config
	}{
		{
			description:          "loadRelative",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				Description: "gosec custom rules configuration",
				Timeout:     60,
				Passthrough: []Passthrough{
					{
						Type:   PassthroughFile,
						Value:  "gosec-sast-config.json",
						Target: "gosec-sast-config.json",
						Mode:   passthroughModeOverwrite,
					},
				},
			},
		},
		{
			description:          "loadRemote",
			rulePath:             "",
			ruleRef:              customRulesetRawRef,
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				TargetDir: "/tmp/glsastrulesetref",
				Timeout:   60,
				Passthrough: []Passthrough{{
					Mode:   passthroughModeOverwrite,
					Type:   PassthroughRaw,
					Target: "/tmp/glsastrulesetref/semgrep-rules.yml",
					Value: `{
    "G104": {
        "html/template.Template": ["Execute"]
    }
}
`,
				}},
			},
		},
		{
			description:          "loadRelative gets priority over loadRemote",
			rulePath:             "testdata/sast-ruleset.toml",
			ruleRef:              customRulesetRawRef,
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				Description: "gosec custom rules configuration",
				Timeout:     60,
				Passthrough: []Passthrough{
					{
						Type:   PassthroughFile,
						Value:  "gosec-sast-config.json",
						Target: "gosec-sast-config.json",
						Mode:   passthroughModeOverwrite,
					},
				},
			},
		},
		{
			description:          "enableLocalConfig=true, with remote config",
			rulePath:             "testdata/sast-ruleset.toml",
			ruleRef:              customRulesetRawRef,
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				Description: "gosec custom rules configuration",
				Timeout:     60,
				Passthrough: []Passthrough{
					{
						Type:   PassthroughFile,
						Value:  "gosec-sast-config.json",
						Target: "gosec-sast-config.json",
						Mode:   passthroughModeOverwrite,
					},
				},
			},
		},
		{
			description:          "enableLocalConfig=false with remote config",
			rulePath:             "testdata/sast-ruleset.toml",
			ruleRef:              customRulesetRawRef,
			enableLocalConfig:    "false",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				TargetDir: "/tmp/glsastrulesetref",
				Timeout:   60,
				Passthrough: []Passthrough{{
					Mode:   passthroughModeOverwrite,
					Type:   PassthroughRaw,
					Target: "/tmp/glsastrulesetref/semgrep-rules.yml",
					Value: `{
    "G104": {
        "html/template.Template": ["Execute"]
    }
}
`,
				}},
			},
		},
		{
			description:          "enableLocalConfig=false without remote config",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			enableLocalConfig:    "false",
		},
		{
			description:          "custom rulesets not enabled",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "gosec",
			enableCustomRulesets: false,
		},
		{
			description:          "no ref or file",
			rulePath:             "testdata/a-made-up-path.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
		},
		{
			description:          "no matching analyzer",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "somethingelse",
			enableCustomRulesets: true,
		},
		{
			description:          "invalid config",
			rulePath:             "testdata/misconfigured.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			if tt.enableCustomRulesets {
				t.Setenv(EnvVarGitlabFeatures, GitlabFeatureCustomRulesetsSAST)
			}

			t.Setenv(GitReferenceSAST, tt.ruleRef)
			t.Setenv(EnableLocalConfiguration, tt.enableLocalConfig)

			cfg, err := Load(tt.rulePath, tt.analyzer, Logger{})
			require.NoError(t, err)

			if tt.wantRule != nil {
				assert.Equal(t, len(tt.wantRule.Passthrough), len(cfg.Passthrough))
				assert.Equal(t, tt.wantRule.Passthrough[0].Value, cfg.Passthrough[0].Value)
			}
		})
	}
}

func TestErrorFromLoad(t *testing.T) {
	t.Setenv(EnvVarGitlabFeatures, GitlabFeatureCustomRulesetsSAST)

	_, err := Load(t.TempDir(), "gosec", Logger{})
	// attempting to write to a directory results in an error
	require.ErrorIs(t, err, syscall.EISDIR)
}

func TestLoadRelative(t *testing.T) {
	var tests = []struct {
		enableCustomRulesets  bool
		enablePassthroughTest bool
		description           string
		rulePath              string
		analyzer              string
		wantErr               error
		wantRule              *Config
	}{
		{
			description:           "valid semgrep analyzer ruleset",
			rulePath:              "testdata/sast-ruleset.toml",
			analyzer:              "semgrep",
			enableCustomRulesets:  true,
			enablePassthroughTest: true,
			wantRule: &Config{
				TargetDir:   "/tmp/sgrules",
				Timeout:     60,
				Interpolate: true,
				Validate:    false,
				Description: "semgrep custom rules configuration",
				Passthrough: []Passthrough{
					{
						Type:   PassthroughGit,
						Value:  "https://gitlab.com/julianthome/semgrep-rules",
						Subdir: "go",
						Target: "/tmp/sgrules",
						Ref:    "refs/heads/develop",
						Mode:   passthroughModeOverwrite,
					},
					{
						Type:   PassthroughGit,
						Value:  "https://gitlab.com/gitlab-org/security-products/sast-rules",
						Target: "/tmp/sgrules",
						Ref:    "97f7686db058e2141c0806a477c1e04835c4f395",
						Mode:   passthroughModeOverwrite,
					},
					{
						Type:   PassthroughRaw,
						Value:  "semgrep rule",
						Target: "/tmp/sgrules/out.yml",
						Mode:   passthroughModeOverwrite,
					},
					{
						Type:      PassthroughFileURL,
						Value:     "https://semgrep.dev/c/p/gosec",
						Target:    "/tmp/sgrules/gosec.yml",
						Mode:      passthroughModeOverwrite,
						Validator: ValidatorYAML,
					},
				},
			},
		},
		{
			description:          "valid semgrep analyzer ruleset with file passthrough",
			rulePath:             "testdata/sast-rules-semgrep-with-single-file-passthrough.toml",
			analyzer:             "semgrep",
			enableCustomRulesets: true,
			wantRule: &Config{
				TargetDir:   "/sgrules",
				Path:        "testdata/sast-rules-semgrep-with-single-file-passthrough.toml",
				Description: "semgrep custom rules configuration",
				Timeout:     60,
				Passthrough: []Passthrough{
					{
						Type:   PassthroughFile,
						Value:  "foo.yml",
						Subdir: "",
						Target: "/sgrules/rule.yml",
						Mode:   passthroughModeAppend,
					},
				},
			},
		},
		{
			description:          "valid gosec analyzer ruleset",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				Description: "gosec custom rules configuration",
				Timeout:     60,
				Path:        "testdata/sast-ruleset.toml",
				Passthrough: []Passthrough{
					{
						Type:   PassthroughFile,
						Value:  "gosec-sast-config.json",
						Target: "gosec-sast-config.json",
						Mode:   passthroughModeOverwrite,
					},
				},
			},
		},
		{
			description:          "invalid analyzer",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "gobadsec",
			enableCustomRulesets: true,
			wantErr: &ConfigNotFoundError{
				Analyzer:    "gobadsec",
				RulesetPath: "testdata/sast-ruleset.toml",
			},
		},
		{
			description:          "invalid rulePath",
			rulePath:             "testdata/sast-no-rules-here.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantErr: &ConfigFileNotFoundError{
				RulesetPath: "testdata/sast-no-rules-here.toml",
			},
		},
		{
			description:          "valid spotbugs analyzer ruleset",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "spotbugs",
			enableCustomRulesets: true,
			wantRule: &Config{
				Timeout:     60,
				TargetDir:   "/spotbugs",
				Description: "spotbugs",
				Path:        "testdata/sast-ruleset.toml",
				Passthrough: []Passthrough{
					{
						Type:   PassthroughRaw,
						Target: "/spotbugs/settings.xml",
						Value:  "spotbugs setting value",
						Mode:   passthroughModeOverwrite,
					},
					{
						Type:   PassthroughRaw,
						Target: "/spotbugs/filter.xml",
						Value:  "spotbugs filter value",
						Mode:   passthroughModeOverwrite,
					},
				},
			},
		},
		{
			description:          "valid gosec analyzer ruleset but disabled",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "gosec",
			enableCustomRulesets: false,
			wantErr:              &NotEnabledError{},
		},
		{
			description:          "misconfigured toml",
			rulePath:             "testdata/misconfigured.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantErr: &InvalidConfig{
				RulesetPath: "testdata/misconfigured.toml",
				Err:         errors.New("(1, 2): unexpected token table key cannot contain ']', was expecting a table key"),
			},
		},
		{
			description:          "valid gosec analyzer ruleset with disabled rulesets",
			rulePath:             "testdata/sast-ruleset-disable-ids.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				Description: "gosec custom rules configuration",
				Timeout:     60,
				Path:        "testdata/sast-ruleset-disable-ids.toml",
				Ruleset: []Ruleset{
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "CWE-1",
						},
						Disable: true,
					},
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "Gosec Rule ID G203",
						},
						Disable: true,
					},
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "Gosec Rule ID G204",
						},
						Disable: false,
					},
				},
			},
		},
		{
			description:          "valid gosec analyzer ruleset with invalid disabled flag scope",
			rulePath:             "testdata/sast-ruleset-disable-ids-with-bad-disable-scope.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				Description: "gosec custom rules configuration",
				Timeout:     60,
				Path:        "testdata/sast-ruleset-disable-ids-with-bad-disable-scope.toml",
				Ruleset: []Ruleset{
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "CWE-1",
						},
					},
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "Gosec Rule ID G203",
						},
					},
				},
			},
		},
	}

	// Disable custom ruleset feature availability
	oldval, err := setFeatureEnv("")
	if err != nil {
		t.Fatal(err)
	}
	defer setFeatureEnv(oldval)

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			if test.enableCustomRulesets {
				setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
			}

			rule, err := LoadRelative(test.rulePath, test.analyzer)
			if err != nil && test.wantErr == nil {
				t.Fatalf("Unable to load %s: %s", test.rulePath, err.Error())
			}

			if test.wantErr != nil {
				errEqual(t, err, test.wantErr)
			}

			if test.enablePassthroughTest {
				for _, pt := range rule.Passthrough {
					_, err := processPassthrough(pt, Logger{})
					if err != nil {
						t.Error(err)
					}
				}
			}

			require.Equal(t, test.wantRule, rule)

			// clear feature
			setFeatureEnv("")
		})
	}
}

func TestLoadRemote(t *testing.T) {
	// Only run authentication tests if token is set
	GlAuth := os.Getenv("GITLAB_AUTH")
	authEnabled := false
	if GlAuth != "" {
		authEnabled = true
	}

	var tests = []struct {
		description     string
		authenticate    bool
		rulesetRef      string
		analyzer        string
		wantErr         string
		fuzzyValueMatch bool
		wantConfig      *Config
	}{
		{
			description:     "loads a configuration from an unauthenticated remote with no Ref semgrep",
			rulesetRef:      "gitlab.com/gitlab-org/security-products/demos/custom-rulesets-for-semgrep-js",
			analyzer:        "semgrep",
			fuzzyValueMatch: true,
			wantConfig: &Config{
				Passthrough: []Passthrough{{
					Mode:  passthroughModeOverwrite,
					Type:  PassthroughFile,
					Value: "semgrep-rules.yml",
				}},
			},
		},
		{
			description:     "loads a configuration from an unauthenticated remote with no Ref secrets",
			rulesetRef:      "gitlab.com/gitlab-org/security-products/demos/custom-rulesets-for-secrets",
			analyzer:        "secrets",
			fuzzyValueMatch: true,
			wantConfig: &Config{
				Ruleset: []Ruleset{
					{
						Identifier: Identifier{
							Type:  "gitleaks_rule_id",
							Value: "Slack Webhook",
						},
						Disable: true,
					},
					{
						Identifier: Identifier{
							Type:  "gitleaks_rule_id",
							Value: "Ionic API token",
						},
						Disable: true,
					},
				},
			},
		},
		{
			description:     "loads a configuration from an unauthenticated remote with Ref",
			rulesetRef:      customRulesetRawRef,
			analyzer:        "gosec",
			fuzzyValueMatch: false,
			wantConfig: &Config{
				Passthrough: []Passthrough{{
					Mode:   passthroughModeOverwrite,
					Type:   PassthroughRaw,
					Target: "semgrep-rules.yml",
					Value: `{
    "G104": {
        "html/template.Template": ["Execute"]
    }
}
`,
				}},
			},
		},
		{
			description:  "attempts to load a configuration from an authenticated private remote without Ref",
			authenticate: authEnabled,
			rulesetRef:   GlAuth + "@gitlab.com/gitlab-org/security-products/tests/go-private",
			analyzer:     "gosec",
			wantErr:      "not found", // None on default branch
		},
		{
			description:     "loads a configuration from an authenticated private remote with Ref",
			authenticate:    authEnabled,
			rulesetRef:      GlAuth + "@gitlab.com/gitlab-org/security-products/tests/go-private@2488eaa671cc02ecc73cddffc5a2b4c528469933",
			analyzer:        "gosec",
			fuzzyValueMatch: false,
			wantConfig: &Config{
				Passthrough: []Passthrough{{
					Mode:   passthroughModeOverwrite,
					Type:   PassthroughRaw,
					Target: "semgrep-rules.yml",
					Value: `{
    "G104": {
        "html/template.Template": ["Execute"]
    }
}
`,
				}},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			t.Setenv(EnvVarGitlabFeatures, GitlabFeatureCustomRulesetsSAST)

			got, err := loadRemote(tt.rulesetRef, tt.analyzer, Logger{})
			if err != nil {
				if !tt.authenticate {
					assert.Error(t, err)
					assert.Contains(t, err.Error(), "authentication required")
					return
				}

				if tt.wantErr == "" {
					t.Fatal(err)
				}
			}

			if tt.wantErr != "" {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), tt.wantErr)
				return
			}

			assert.Len(t, got.Passthrough, len(tt.wantConfig.Passthrough))

			for i := range got.Passthrough {
				assert.Equal(t, tt.wantConfig.Passthrough[i].Mode, got.Passthrough[i].Mode)
				assert.Equal(t, tt.wantConfig.Passthrough[i].Type, got.Passthrough[i].Type)

				if tt.fuzzyValueMatch {
					tmpRegex := regexp.MustCompile(`/tmp/glsastrulesetremoteref\w+`)
					assert.Regexp(t, tmpRegex, got.Passthrough[i].Value)
				} else {
					assert.Equal(t, tt.wantConfig.Passthrough[i].Value, got.Passthrough[i].Value)
				}
			}

			if len(tt.wantConfig.Ruleset) == 0 {
				return
			}

			assert.Len(t, got.Ruleset, len(tt.wantConfig.Ruleset))
			for i := range got.Ruleset {
				assert.Equal(t, tt.wantConfig.Ruleset[i].Identifier.Type, got.Ruleset[i].Identifier.Type)
				assert.Equal(t, tt.wantConfig.Ruleset[i].Identifier.Value, got.Ruleset[i].Identifier.Value)
				assert.Equal(t, tt.wantConfig.Ruleset[i].Disable, got.Ruleset[i].Disable)
			}

		})
	}
}

// Returns true if `basePath` contains `targetPath`, unless `basePath` is "/" and the paths are a mix of relative and absolute.
func pathContains(basepath, targpath string) bool {
	if basepath == "/" {
		return true
	}

	rel, err := filepath.Rel(basepath, targpath)
	if err != nil {
		return false
	}
	return !strings.HasPrefix(rel, "../")
}
func TestPathContains(t *testing.T) {
	for _, tt := range []struct {
		basepath string
		targpath string
		contains bool
	}{
		{"", "a", true},
		{"a", "", false},
		{"/", "a", true},
		{"/a", "", false},
		{"", "/a", false},
		{"a", "/", false},

		{"a", "a", true},
		{"a", "a/b", true},
		{"a", "b", false},
		{"a", "b/a", false},

		{"/a", "/a", true},
		{"/a", "/a/b", true},
		{"/a", "/b", false},
		{"/a", "/b/a", false},

		{"/a", "a", false},
		{"/a", "a/b", false},
		{"/a", "b", false},
		{"/a", "b/a", false},

		{"a", "/a", false},
		{"a", "/a/b", false},
		{"a", "/b", false},
		{"a", "/b/a", false},
	} {
		t.Run(fmt.Sprintf(`("%s","%s")`, tt.basepath, tt.targpath), func(t *testing.T) {
			require.Equal(t, tt.contains, pathContains(tt.basepath, tt.targpath))
		})
	}
}

func TestLoadRemoteDoesNotTargetClonedRepo(t *testing.T) {
	t.Setenv(EnvVarGitlabFeatures, GitlabFeatureCustomRulesetsSAST)

	for _, tt := range []struct {
		ref      string
		analyzer string
	}{
		{customRulesetRawRef, "gosec"},
		{customRulesetSynthesisRef, "semgrep"},
	} {
		t.Run(fmt.Sprintf("ref: %s analyzer: %s", tt.ref, tt.analyzer), func(t *testing.T) {
			got, err := loadRemote(tt.ref, tt.analyzer, Logger{})
			require.NoError(t, err)

			require.True(t, strings.HasSuffix(got.Path, PathSAST), "PathSAST should be a suffix of got.Path")
			cloneDir := strings.TrimSuffix(got.Path, PathSAST)

			require.False(t, pathContains(got.TargetDir, cloneDir))
		})
	}
}

func TestSetSensibleDefaultsPrependsSourceToFilePassthroughs(t *testing.T) {
	t.Setenv(EnvVarGitlabFeatures, GitlabFeatureCustomRulesetsSAST)

	filePassthroughAnalyzer := "gosec"
	filePassthroughValue := "semgrep-rules.yml"
	filePassthroughConfig := Config{
		Passthrough: []Passthrough{{
			Mode:   passthroughModeOverwrite,
			Type:   PassthroughFile,
			Target: filePassthroughValue,
			Value:  filePassthroughValue,
		}},
	}
	gitPassthroughAnalyzer := "semgrep"
	gitPassthroughValue := "https://gitlab.com/gitlab-org/security-products/sast-rules"
	gitPassthroughConfig := Config{
		Passthrough: []Passthrough{{
			Mode:  passthroughModeOverwrite,
			Type:  PassthroughGit,
			Value: gitPassthroughValue,
			Ref:   "refs/heads/main",
		}},
	}

	configs := map[string]Config{}
	configs[filePassthroughAnalyzer] = filePassthroughConfig
	configs[gitPassthroughAnalyzer] = gitPassthroughConfig

	setSensibleDefaults(configs, "", "")

	assert.Equal(t, filePassthroughValue, configs[filePassthroughAnalyzer].Passthrough[0].Value)
	assert.Equal(t, gitPassthroughValue, configs[gitPassthroughAnalyzer].Passthrough[0].Value)

	setSensibleDefaults(configs, "/tmp", "/src")

	assert.Equal(t, filepath.Join("/src", filePassthroughValue), configs[filePassthroughAnalyzer].Passthrough[0].Value)
	assert.Equal(t, gitPassthroughValue, configs[gitPassthroughAnalyzer].Passthrough[0].Value)
}

func TestLoadRulesetFeatureUnsupported(t *testing.T) {
	// Disable custom ruleset feature availability
	oldval, err := setFeatureEnv("")
	if err != nil {
		t.Fatal(err)
	}
	defer setFeatureEnv(oldval)

	rule, err := LoadRelative("testdata/sast-ruleset.toml", "gosec")
	if err == nil {
		t.Fatal("Load should have returned an err")
	}
	const expectedErr = "ruleset customization not enabled"
	if err.Error() != expectedErr {
		t.Fatalf("expected error: %v \ngot: %v", expectedErr, err)
	}

	if rule != nil {
		t.Errorf("Wrong result. Expecting nil for rule.\nGot:\n%v", rule)
	}
}

func TestFormatGitUri(t *testing.T) {
	tests := []struct {
		name string
		url  string
		auth string
		want string
	}{
		{
			name: "no git suffix",
			url:  "gitlab.com/gitlab-org/security-products/tests/go",
			auth: "myusername:mypassword",
			want: "https://myusername:mypassword@gitlab.com/gitlab-org/security-products/tests/go.git",
		},
		{
			name: "with git suffix",
			url:  "https://gitlab.example.com.git",
			auth: "myusername:mypassword",
			want: "https://myusername:mypassword@gitlab.example.com.git",
		},
		{
			name: "no auth",
			url:  "gitlab.example.com.git",
			auth: "",
			want: "https://gitlab.example.com.git",
		},
		{
			name: "hostname starting with an h",
			url:  "hello.gitlab.example.com.git",
			auth: "",
			want: "https://hello.gitlab.example.com.git",
		},
		{
			name: "hostname starting with a t",
			url:  "try.gitlab.example.com.git",
			auth: "",
			want: "https://try.gitlab.example.com.git",
		},
		{
			name: "hostname starting with a p",
			url:  "please.gitlab.example.com.git",
			auth: "",
			want: "https://please.gitlab.example.com.git",
		},
		{
			name: "hostname starting with an s",
			url:  "some.gitlab.example.com.git",
			auth: "",
			want: "https://some.gitlab.example.com.git",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := formatGitURI(tt.url, tt.auth)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestInvalidPassthroughFileURL(t *testing.T) {
	pt := Passthrough{
		Type:  PassthroughFileURL,
		Value: "https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/raw/main/rules/nope.yml",
	}

	cfgmap := map[string]Config{
		"cfg": {
			Path:        "/tmp/sg-rules",
			Passthrough: []Passthrough{pt},
		},
	}
	setSensibleDefaults(cfgmap, "", "")

	c, _ := cfgmap["cfg"]
	_, err := ProcessPassthroughs(&c, Logger{})

	if err.Error() != "requested URL \"https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/raw/main/rules/nope.yml\" returned a non-200 response status (404)" {
		t.Fatalf("expected URL error but got %q", err)
	}
}

func TestPassthroughRaw(t *testing.T) {
	pt := Passthrough{
		Type:   PassthroughRaw,
		Target: "rule.yml",
		Value:  "filecontents",
	}

	got, err := passthroughRaw(pt, Logger{})
	if err != nil {
		t.Fatalf("passthroughRaw() for raw: %v, unexpected error: %v", pt.Value, err)
	}
	defer os.Remove(got)

	if _, err := os.Stat(got); err != nil {
		t.Fatalf("Expected tempfile to exist, but got: %v", err)
	}
	dat, err := ioutil.ReadFile(got)
	if err != nil {
		t.Fatalf("passthroughRaw() tempfile cannot be read, unexpected error: %v", err)
	}
	if string(dat) != pt.Value {
		t.Fatalf("passthroughRaw() tempfile does not match raw: %v", pt.Value)
	}
}

func TestCleanPath(t *testing.T) {
	tests := []struct {
		name string
		path string
		want string
	}{
		{
			name: "undecorated path",
			path: "gosec-config.json",
			want: "gosec-config.json",
		},
		{
			name: "dot decorated path",
			path: "./gosec-config.json",
			want: "gosec-config.json",
		},
		{
			name: "double-dot decorated path",
			path: "../gosec-config.json",
			want: "gosec-config.json",
		},
		{
			name: "double-dot decorated nested path",
			path: "./foo/bar/../../gosec-config.json",
			want: "gosec-config.json",
		},
		{
			name: "absolute path",
			path: "/tmp/config.json",
			want: "/tmp/config.json",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := cleanPath(tt.path)
			require.Equal(t, tt.want, got)
		})
	}
}

func TestCustomRulesetEnabled(t *testing.T) {
	tests := []struct {
		enabled     bool
		rulesetPath string
		want        bool
	}{
		{
			enabled:     true,
			rulesetPath: "testdata/sast-ruleset.toml",
			want:        true,
		},
		{
			enabled:     false,
			rulesetPath: "testdata/sast-ruleset.toml",
			want:        false,
		},
	}

	// Disable custom ruleset feature availability
	oldval, err := setFeatureEnv("")
	if err != nil {
		t.Fatal(err)
	}
	defer setFeatureEnv(oldval)

	for _, test := range tests {
		if test.enabled {
			setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
		}
		if got := customRulesetEnabled(); got != test.want {
			t.Errorf("Wrong result. Expecting %v for custRulesetEnabled. Got:%v", test.want, got)
		}
		// clear feature
		setFeatureEnv("")
	}
}

func TestDisabledIdentifiers(t *testing.T) {
	var tests = []struct {
		enabled         bool
		description     string
		rulePath        string
		wantErr         error
		analyzer        string
		wantDisabledIDs map[string]bool
		rulesetPath     string
	}{
		{
			enabled:         true,
			analyzer:        "gosec",
			description:     "valid gosec analyzer ruleset",
			wantDisabledIDs: map[string]bool{"CWE-CWE-1": true, "CWE-Gosec Rule ID G203": true},
			rulesetPath:     "testdata/sast-ruleset-disable-ids.toml",
		},
		{
			enabled:     true,
			analyzer:    "gosec",
			description: "valid gosec analyzer ruleset",
			wantErr: &ConfigFileNotFoundError{
				RulesetPath: "testdata/sast-ruleset-bad-path-disable-ids.toml",
			},
			wantDisabledIDs: map[string]bool{},
			rulesetPath:     "testdata/sast-ruleset-bad-path-disable-ids.toml",
		},
		{
			enabled:     true,
			analyzer:    "gobadsec",
			description: "valid gosec analyzer ruleset",
			wantErr: &ConfigNotFoundError{
				Analyzer:    "gobadsec",
				RulesetPath: "testdata/sast-ruleset-disable-ids.toml",
			},
			wantDisabledIDs: map[string]bool{},
			rulesetPath:     "testdata/sast-ruleset-disable-ids.toml",
		},
		{
			enabled:         false,
			analyzer:        "gosec",
			description:     "valid gosec analyzer ruleset",
			wantErr:         &NotEnabledError{},
			wantDisabledIDs: map[string]bool{},
			rulesetPath:     "testdata/sast-ruleset-disable-ids.toml",
		},
	}

	oldval, err := setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
	if err != nil {
		t.Fatal(err)
	}
	defer setFeatureEnv(oldval)

	for _, test := range tests {
		if test.enabled {
			setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
		}
		// Enable custom ruleset feature availability

		rulesetConfig, err := LoadRelative(test.rulesetPath, test.analyzer)
		if err != nil && test.wantErr == nil {
			t.Fatalf("Unable to load %s: %s", test.rulesetPath, err.Error())
		}

		errEqual(t, err, test.wantErr)

		if rulesetConfig != nil {
			disabledIDs, err := rulesetConfig.DisabledIdentifiers()
			errEqual(t, err, test.wantErr)

			require.Equal(t, test.wantDisabledIDs, disabledIDs)
		}
		setFeatureEnv("")
	}
}

func TestIdentifiersWithOverrides(t *testing.T) {
	var tests = []struct {
		enabled           bool
		description       string
		wantErr           error
		analyzer          string
		wantOverriddenIDs map[string]Ruleset
		rulesetPath       string
	}{
		{
			enabled:     false,
			analyzer:    "gosec",
			description: "valid gosec analyzer ruleset",
			wantOverriddenIDs: map[string]Ruleset{"CWE-CWE-79": {
				Identifier: Identifier{
					Type:  "CWE",
					Value: "CWE-79",
				},
				Override: Override{
					Name:        "OVERRIDDEN name",
					Message:     "OVERRIDDEN message",
					Description: "OVERRIDDEN description",
					Severity:    "Critical",
				},
			}},
			rulesetPath: "testdata/sast-ruleset-overrides.toml",
		},
	}

	oldval, err := setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
	if err != nil {
		t.Fatal(err)
	}
	defer setFeatureEnv(oldval)

	for _, test := range tests {
		if test.enabled {
			setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
		}
		// Enable custom ruleset feature availability

		rulesetConfig, err := LoadRelative(test.rulesetPath, test.analyzer)

		if err != nil && test.wantErr == nil {
			t.Fatalf("Unable to load %s: %s", test.rulesetPath, err.Error())
		}

		identifiersWithOverrides, err := rulesetConfig.IdentifiersWithOverrides()
		errEqual(t, err, test.wantErr)

		require.Equal(t, test.wantOverriddenIDs, identifiersWithOverrides)
		setFeatureEnv("")
	}
}

func TestProcessPassthrough(t *testing.T) {
	tempDir := t.TempDir()
	targetDir := filepath.Join(tempDir, "one/two/three")
	targetFile := filepath.Join(targetDir, "some-file.txt")

	passthrough := Passthrough{
		Type:   PassthroughRaw,
		Value:  "passthrough-value",
		Target: targetFile,
	}

	_, err := processPassthrough(passthrough, Logger{})
	require.NoError(t, err)

	tests := []struct {
		description     string
		path            string
		wantPermissions int
	}{
		{
			description:     "Test permissions of created dir",
			path:            targetDir,
			wantPermissions: 0o700,
		},
		{
			description:     "Test permissions of created file",
			path:            targetFile,
			wantPermissions: 0o644,
		},
	}

	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			info, err := os.Stat(tt.path)
			require.NoError(t, err)

			want := fs.FileMode(tt.wantPermissions)
			got := info.Mode().Perm()

			require.Equalf(t, want, got,
				"expected path %q to have permissions %#o, got %#o", tt.path, want, got)
		})
	}
}

func TestPassthroughTargetDirPermissions(t *testing.T) {
	tempDir := t.TempDir()
	targetDir := filepath.Join(tempDir, "one/two/three")

	cfg := Config{
		Timeout:     0,
		TargetDir:   targetDir,
		Passthrough: []Passthrough{},
	}
	_, err := ProcessPassthroughs(&cfg, Logger{})
	require.NoError(t, err)

	info, err := os.Stat(cfg.TargetDir)
	require.NoError(t, err)

	want := fs.FileMode(0700)
	got := info.Mode().Perm()

	require.Equalf(t, want, got,
		"expected cfg.TargetDir %q to have permissions %#o, got %#o", cfg.TargetDir, want, got)
}
