package ruleset

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseRulesetRef(t *testing.T) {
	var tests = []struct {
		name        string
		refString   string
		passthrough *Passthrough
		wantErr     bool
	}{
		{
			name:      "Valid authenticated URI with ref and simple password",
			refString: "myuser:mypassword@gitlab.com/company-a/project-b@a1b2c3d4",
			passthrough: &Passthrough{
				Value: "gitlab.com/company-a/project-b",
				Ref:   "a1b2c3d4",
				Auth:  "myuser:mypassword",
			},
		},
		{
			name:      "Valid authenticated URI with ref and special char password",
			refString: "my.user:my.p@ssword@gitlab.com/company-a/project-b@a1b2c3d4",
			passthrough: &Passthrough{
				Value: "gitlab.com/company-a/project-b",
				Ref:   "a1b2c3d4",
				Auth:  "my.user:my.p@ssword",
			},
		},
		{
			name:      "Valid unauthenticated URI with SHA ref",
			refString: "about.gitlab.com/company-a/project-b@a1b2c3d4",
			passthrough: &Passthrough{
				Value: "about.gitlab.com/company-a/project-b",
				Ref:   "a1b2c3d4",
				Auth:  "",
			},
		},
		{
			name:      "Valid unauthenticated URI with tag ref",
			refString: "about.gitlab.com/company-a/project-b@v1.2.3",
			passthrough: &Passthrough{
				Value: "about.gitlab.com/company-a/project-b",
				Ref:   "v1.2.3",
				Auth:  "",
			},
		},
		{
			name:      "Valid unauthenticated URI with special char ref",
			refString: "about.gitlab.com/company-a/project-b@my/branch_name.is-complex",
			passthrough: &Passthrough{
				Value: "about.gitlab.com/company-a/project-b",
				Ref:   "my/branch_name.is-complex",
				Auth:  "",
			},
		},
		{
			name:      "Valid authenticated URI without ref",
			refString: "myuser:mypassword@gitlab.com/company-a/project-b",
			passthrough: &Passthrough{
				Value: "gitlab.com/company-a/project-b",
				Auth:  "myuser:mypassword",
			},
		},
		{
			name:      "Invalid too few parts",
			refString: "@",
			wantErr:   true,
		},
		{
			name:      "Invalid missing URI",
			refString: "myuser:mypassword@@a1b2c3d4",
			wantErr:   true,
		},
		{
			name:      "Invalid authentication with incomplete password",
			refString: "myuser:@gitlab.com",
			wantErr:   true,
		},
		{
			name:      "Invalid authentication without incomplete username",
			refString: ":mypassword@gitlab.com",
			wantErr:   true,
		},
		{
			name:      "Invalid unnauthenticated URI with port and ref",
			refString: "gitlab.com:8888/company-a/project-b@a1b2c3d4",
			wantErr:   true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pt, err := parseRulesetRef(tt.refString)
			if err != nil {
				if tt.wantErr != true {
					t.Fatalf("Wrong result. Unexpected err %v", err)
				}
			} else {
				require.Equal(t, tt.passthrough, pt)
			}
		})
	}
}
