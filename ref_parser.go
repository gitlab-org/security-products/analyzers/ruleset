package ruleset

import (
	"errors"
	"regexp"
	"strings"
)

// SCP-style regex that relies on 3 groupings:
// - Auth (optional, but must contain colon)
// - URL (required)
// - Ref (optional)
//
// If colon exists, assume to be auth. Port references are not currently supPORTed.
var validRulesetRef = regexp.MustCompile(`(?:(.+:.+)@){0,1}([\w\-\.]*[\w\-]+\.[\w\/\-]+)(?:@*([\w\.\-_\/]+)){0,1}`)

// parseRulesetRef parses a ruleset reference string into a Passthrough containing
// an Auth, Value, and Ref property. These are used to fetch a remote
// ruleset using an SCP-style reference;
//
// For example:
//
// "username:password@gitlab.example.com/group/project@v1.1.1
//
// Returns:
//
//	Passthrough{
//		Auth: "username:password",
//		Value: "gitlab.example.com/group/project",
//		Ref: "v1.1.1",
//	}
func parseRulesetRef(refString string) (*Passthrough, error) {
	parts := validRulesetRef.FindStringSubmatch(refString)

	if len(parts) != 4 {
		return nil, errors.New("Shared ruleset reference doesn't have the right format")
	}

	// Return invalid refs if auth contains missing components
	if strings.Contains(refString, ":") && parts[1] == "" {
		return nil, errors.New("Shared ruleset reference includes ':' but doesn't have a valid username and password")
	}

	return &Passthrough{
		Auth:  parts[1],
		Value: parts[2],
		Ref:   parts[3],
	}, nil
}
